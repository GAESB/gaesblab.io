# Pedidos

```{note}
Posible pedido conjunto a Landjoff:
- Ibone
- Jon
- Unai
```

Como referencia para futuras compras, a continuación se indican algunos pedidos conjuntos realizados por miembros del
grupo.

## 2024/02/15 Machay

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| CO5DYN | Cordino dyneema 5 mm Beal | 4,27€ | 5 | 21,33€ |
| OC550 | DESCENSOR OCTO QI´ROC (COLOR: Verde) | 9,99€ | 1 | 9,99€ |
| HM65R | Frontal HM65R Fenix 1400 lumens | 85,47€ | 2 | 170,94€ |
| 508-7 | Maillon ovalado cincado FIXE (Diámetro: 7 mm) | 3,30€ | 1 | 3,30€ |
| 126301 | Mosqueton Orbit lock CAMP (COLOR: Naranja) | 10,61€ | 2 | 21,22€ |
| xrope marker | Rotulador cuerdas tendon | 4,80€ | 1 | 4,80€ |
| 90181/001 | Tubo termoretractil para terminación de cuerdas Kordas | 4,17€ | 1 | 4,17€ |
| | | | | **235,75€** |

## 2023/12/19 Barrabes

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| | Grivel K8G Sigma Twin Gate | 13.38€ | x2 | 26,76€ |
| | | | | **26,76€** |

## 2023/12/19 Barrabes

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| | Grivel Lamda K7G | 14,95€ | -1 | -14,95€ |
| | Petzl Djinn Recto Gris | 8,01€ | 2 | 16,02€ |
| | Petzl Omni Triact-Lock | 26,16€ | 2 | 52,32€ |
| | | | | **53,39€** |

## 2023/12/15 Barrabes

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| | Grivel K8G Sigma Twin Gate | 13,38€ | 1 | 13,38€ |
| | Grivel Lamda K7G | 14,95€ | 1 | 14,95€ |
| | | | | **28,33€** |

## 2023/12/15 Aventuramania

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| PTZB16BAA | Bloqueador Petzl Croll S | 35,95€ | 2 | 71,90€ |
| PTZB18BAA | Bloqueador Petzl Basic | 36,95€ | 3 | 110,85€ |
| PTZD009AA00 | Descensor Petzl Stop | 89,95€ | 3 | 269,85€ |
| | Total productos | | | 452,60€ |
| | Gastos de envío | | | 6,99€ |
| | | | | **459,59€** |

## 2023/11/27 Machay

| Referencia | Producto | Precio | Unidades | Subtotal |
|---|---|---|---|---|
| B6120 | Arnés Amazonia MTDE | 67,87€ | 2 | 135,74€ |
| | CUERDA DINAMICA MONKEY 9MM FIXE POR METROS - COLOR: Violeta | 2,86€ | 5 | 14,30€ |
| | CUERDA DINAMICA MONKEY 9MM FIXE POR METROS - COLOR: Amarillo | 2,86€ | 5 | 14,30€ |
| 163 | Mosqueton de freno handy Raumer | 21,82€ | 3  | 65,46€ |
| 5070110 | Bolsa impermeable altus talla S | 4,32€ | 1 | 4,32€ |
| ALGEL | ALCOHOL EN GEL 200 gr | 2,39€ | 3 | 7,17€ |
| S92A | Navaja Spatha Petzl - COLOR: Azul | 25,60€ | 1 | 25,60€ |
| CO5DYN | Cordino dyneema 5 mm Beal | 4,27€ | 5 | 21,35€ |
| | | | | **288,29€** |
