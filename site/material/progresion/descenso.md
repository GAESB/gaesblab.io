# Descenso

En todos estos casos, a excepción del último, tenemos el descensor en la cuerda sólo.

## Fraccionamiento

- Descendemos hasta poder anclarnos con el cabo corto al mosquetón del fraccionamiento.
- Hacemos un bloqueo simple del descendedor.
- Anclamos el cabo corto al mosquetón del fraccionamiento.
- Desbloquemos el descendedor y transferimos el peso al cabo corto.
- Ponemos el bloqueador de mano (o sólo el cabo largo) en la cuerda superior.
- Quitamos el descendedor y lo ponemos en la cuerda inferior, transfiriendo el peso al mismo y bloqueándolo por completo.
- Quitamos el cabo corto.
- Desbloqueamos el descendedor y comprobamos que el funcionamiento es correcto.
- Antes de alejarnos en exceso, quitamos el bloqueador de mano (o cabo largo sólo).

## Nudo

- Descendemos hasta que el nudo haga tope.
- Anclamos el cabo corto al nudo.
- Ponemos el bloqueador de mano sobre el descendedor.
- Ponemos el bloqueador ventral sobre el descendedor, y transferimos el peso al mismo.
- Quitamos el descendedor, lo ponemos en la cuerda inferior y lo bloqueamos por completo.
- Quitamos el bloqueador ventral, transfiriendo el peso al descendedor.
- Quitamos el bloqueador de mano.
- Antes de alejarnos en exceso, quitamos el cabo corto.

## Cambio

- Bloqueamos el descendedor por completo.
- Ponemos el bloqueador de mano.
- Anclamos el cabo corto sobre el bloqueador de mano.
- Ponemos el bloqueador ventral y transferimos el peso al mismo.
- Quitamos el descendedor.
- Quitamos el cabo corto.

## Llegada a pasamanos

- Bloqueamos el descendedor por completo.
- Anclamos el cabo corto al primer tramo del pasamanos.
- Ponemos el bloqueador de mano en el primer tramo del pasamanos tan alejado como podamos.
- Desbloqueamos el descendedor, transfiriendo el peso al cabo corto.
- Quitamos el descendedor.

## Salida de pasamanos

Tenemos ambos cabos o el cabo corto y el bloqueador de mano unidos al pasamanos.

- Ponemos el descendedor, transfiriendo el peso al mismo y bloqueándolo por completo.
- Quitamos el cabo corto.
- Desbloqueamos el descendedor y comprobamos que el funcionamiento es correcto.
- Antes de alejarnos en exceso, quitamos el bloqueador de mano.
