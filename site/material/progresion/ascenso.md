# Ascenso

En todos estos casos, a excepción del último, tenemos el bloqueador de mano y el bloqueador ventral en la cuerda, y el pedal unido al bloqueador de mano.

## Fraccionamiento

- Anclamos el cabo corto al mosquetón del fraccionamiento.
  - Si el cabo corto es más largo que el lazo + bloqueador de mano + bloqueador ventral, el peso no se transferirá.
    Tendremos que elevarnos ligeramente a pulso, o cambiar el pedal del bloqueador de mano al mosquetón del fraccionamiento para poder quitar los bloqueadores.
- Quitamos el bloqueador ventral de la cuerda inferior y lo ponemos en la superior tan alejado como podamos.
- Quitamos el bloqueador de mano de la cuerda inferior y lo ponemos en la superior.
- Transferimos el peso al bloqueador ventral.
  - Si hubieramos usado el pedal en el mosquetón de fraccionamiento, volvemos a ponerlo en el bloqueador de mano.
- Antes de alejarnos en exceso, quitamos el cabo corto.

## Nudo

- Anclamos el cabo corto a la coca del nudo.
- Quitamos el bloqueador de mano de la cuerda inferior y lo ponemos en la superior tan alejado como podamos.
- Quitamos el bloqueador ventral de la cuerda inferior y lo ponemos en la superior, ayudándonos para ello del pedal.
  - Al hacerlo, habremos transferido el peso al bloqueador ventral.
- Antes de alejarnos en exceso, quitamos el cabo corto.

## Cambio

- Anclamos el cabo corto encima del bloqueador de mano.
- Ponemos el descendedor debajo del bloqueador ventral y bloqueamos el descendedor por completo.
- Quitamos el bloqueador ventral.
- Transferimos el peso al descendedor.
- Quitamos el cabo corto.
- Quitamos el bloqueador de mano.

## Llegada a pasamanos

- Anclamos el cabo corto al primer tramo del pasamanos.
  - Si el peso no se transfiere al mismo, podemos cambiar el pedal del bloqueador de mano al mosquetón de la cabecera para poder quitar los bloqueadores.
- Quitamos el bloqueador de mano y lo ponemos en el primer tramo del pasamanos tan alejado como podamos.
- Quitamos el bloqueador ventral.
- Antes de alejarnos en exceso, cambiamos el cabo corto al primer tramo del pasamanos.
  - Si hubieramos usado el pedal en el mosquetón de la cabecera, lo recogemos también.

## Salida de pasamanos

Tenemos ambos cabos o el cabo corto y el bloqueador de mano unidos al pasamanos.

- Ponemos el bloqueador ventral en la cuerda de ascenso.
- Quitamos el cabo largo (y el bloqueador de mano) del último tramo del pasamanos, y lo ponemos en la cuerda de ascenso.
- Transferimos el peso a los bloqueadores.
- Antes de alejarnos en exceso, quitamos el cabo corto.
