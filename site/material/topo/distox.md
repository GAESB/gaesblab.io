# DistoX | DistoX2 | DistoXBLE

Ver {doc}`TED:hw/distox`.

Las funciones que más usamos son las siguientes:

- Pulsar DIST para encender y/o activar el láser. Volver a pulsar DIST para tomar la medida.
  - Se puede mantener pulsado el botón hasta que finalice la toma.

- Pulsar UNITS para ver medidas anteriores.
  - Cada vez que se pulsa UNITS, muestra la anterior.
  - También pueden usarse los botones PLUS y MINUS.
  - Nótese que la numeración va hacia atrás. Es decir, la medida 1 es la última, 2 la penúltima, etc.

- Mantener pulsados CLEAR OFF + UNITS durante dos segundos para resetear el contador de medidas
  (no se podrán descargar las que no se hayan descargado antes).

- Pulsar REF brevemente para cambiar la referencia (base, frente) para la siguiente toma.
  - Mantenerlo pulsado al menos dos segundos para cambiarlo para todas las tomas siguientes.
