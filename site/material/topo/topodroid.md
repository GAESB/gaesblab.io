# TopoDroid

Ver {doc}`TED:sw/topodroid`.

Tras instalar TopoDroid-X, hay que realizar los siguientes ajustes para que se adapte a nuestro flujo de trabajo:

- Activity level: Expert
- SURVEY DATA
  - Stations policy: Forward leg, splays
  - Editable stations
- Display Mode: select all
