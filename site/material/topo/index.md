# Topografía espeleológica digital

```{note}
Para una introducción a la topografía espeleológica digital, ver [gaesb.gitlab.io/ted](https://gaesb.gitlab.io/ted/).
En esta sección se detallan las particularidades de los procedimientos utilizados en el GAES.
```

```{toctree}
distox
topodroid
visualtopo
```

## Flujo

- En punta, toma de medidas con {doc}`distox`, descarga frecuente por Bluetooth a {doc}`topodroid`, y bocetado manual.
  - Para trazar la poligonal se realizan tres medidas de un nodo/vértice/estación al siguiente.
  - Después, en el vértice desde el que se han realizado las medidas, se toman cuatro medidas adicionales:
    izquierda, derecha, arriba, abajo.
  - Dependiendo del vértice, puede ser interesante realizar medidas auxiliares para ayudar a trazar el contorno de la
    galería/sala.

- Al salir de la cavidad, o al volver al vivac, se exporta la sesión de TopoDroid en formato ZIP y se transfiere al
  menos a otro dispositivo para tener una copia de seguridad.
  Se comprueba que el dispositivo puede cargarlo correctamente.

- Exportación de {doc}`topodroid` a {doc}`visualtopo` (`.tro`).

- Impresión de poligonal en hoja cuadriculada con los contornos aproximados calculados por VisualTopo.

- Dibujado manual sobre los contornos aproximados utilizando como referencia el bocetado manual y las medidas
  adicionales de referencia.

- Escaneado de los croquis dibujados a mano sobre la impresión de la poligonal de VisualTopo.

- Vectorización manual de los contornos en AutoCAD, utilizando los croquis escaneados como fondo, y utilizando librerías
  de símbolos para definir las caracerísticas.

```{important}
La integración de varias sesiones/secciones se realiza manualmente editando los datos de varios proyectos `.tro` para
unirlos en uno solo y después imprimir el conjunto, o tras la vectorización en AutoCAD.
```

## En punta

Debido a las limitaciones de software del Disto-X, a la imposibilidad de mejorarlo, y a nuestra inexperiencia con el
mismo y con TopoDroid, el procedimiento de toma de medidas en punta requiere revisión constante.

- Al realizar las tres medidas consecutivas de un vértice a otro, tratamos de no variar la posición del Disto-X, ya que
  a menudo resulta difícil encontrar el apoyo correcto.
  Cuando una de ellas nos sale mal, realizamos una cuarta sin variar la posición.

  ```{note}
  No sabemos si se pueden borrar medidas concretas del Disto-X antes de descargarlas a TopoDroid, y por no cometer el
  error de borrarlas todas, dejamos las erróneas en la memoria.

  Cuando cometamos esos errores, ¿deberíamos descargar a TopoDroid y borrar la medida incorrecta en el momento?
  ```

  ```{note}
  Sería interesante tomar cuatro medidas en vez de tres, y colocar el Disto-X en cuatro posiciones, de forma que se
  puedan compensar los errores de calibración en alguno de los ejes.
  Sin embargo, debibo a la forma asimétrica y a la ubicación del botón de disparo, resulta a menudo imposible.
  Se puede utilizar el temporizador para girar el dispositivo 180º si ya se ha encontrado una posición, pero es más
  difícil disparar y después apuntar que primero apuntar y después disparar.
  ```

- En papel, anotamos sólo una medida por cada vértice (la que consideremos mejor), y no apuntamos las medidas
  adicionales ni auxiliares.
  En el boceto se indica la ubicación aproximada y número/identificador de los vértices, pudiendo anotarse medidas
  auxiliares concretas a petición del dibujante (e.g. la altura de una galería en varios puntos entre vértices).

- Cuando llegamos al final transitable de una rama, hay tres opciones:

  - Si podemos acceder al final, realizar tres medidas para definir un vértice final y tomar las medidas adicionales.
  - Si no podemos acceder al final pero tenemos visión del contorno, realizar cinco medidas desde el último vértice al
    centro, izquierda, derecha, arriba, abajo.
  - Si no podemos acceder al final y no tenemos visión del contorno, realizar tres medidas para definir un vértice final
    y no tomar ninguna medida adicional.

- Cuando desandemos una rama y volvamos a tomar tres medidas desde un vértice ya conocido a otro nuevo, deberemos
  descargar el contenido a TopoDroid immediatamente y renombrar el vértice de origen, ya que por defecto tomará el
  último de la rama anterior.

## Revisión, exportación e integración

Antes de exportar los datos de TopoDroid a {doc}`TED:sw/visualtopo` o {doc}`TED:sw/therion`, hay que realizar una
revisión y limpieza.
Por un lado, se eliminan las medidas erróneas y dos de las tres medidas redundantes, para dejar una sola que defina la
poligonal.

```{note}
Iñaki ha comentado que es posible que la herramienta realice una media de las tres medidas, de forma que no sea
necesario eliminar las redundantes.
Pero no tengo claro si lo realiza TopoDroid o VisualTopo, ni cuál es el procedimiento/sintaxis para que lo haga.
Tampoco tengo claro de qué depende que prefiera que lo promedie o no.
```

```{note}
No tengo claro cómo se gestionan las medidas adicionales y las auxiliares.
En la topo del día 13 de enero de 2024, tomé algunas medidas adicionales en el orden inverso (primero derecha y después
izquierda).
Entiendo que el software es capaz de interpretar cuál es cada una por la posición de los vectores con respecto a la
poligonal, ya que son ortogonales.
Pero no tengo la certeza de que funcione siempre que las cuatro medidas adicionales estén immediatamente después de las
tres redundantes, y antes de las auxiliares.
Tampoco tengo claro si el patrón de tomar cuatro medidas adicionales es entendido por TopoDroid o sólo por VisualTopo.
TopoDroid reconoce el patrón "vértice, splays", pero puede que le de igual qué representen los splays y sólo sea
VisualTopo el que lo interprete.
En la topo del día 20 de enero de 2024, creo que tomamos todas las medidas en el orden adecuado (primero izquierda y
después derecha).
Con Iñaki estaría bien revisar si las dimensiones adicionales están bien; si tienen algún sentido a diferencia de las
del 13 de enero.
```

En lo que respecta a la integración, actualmente Iñaki edita manualmente varios ficheros `.tro`/`.trox` para crear uno
nuevo que contenga toda la información.
Por otro lado, mediante {doc}`TED:sw/therion` (ya sea en gabinete o usando el gestor de proyectos de
{doc}`TED:sw/topodroid`), podemos integrar automáticamente varias sesiones.
Sin embargo, todavía sólo sabemos proyectar la poligonal y las medidas radiales/auxiliares, sin contorno aproximado.

## Contorno y vectorización

Actualmente, el trazado de los contornos y la vectorización se realiza de forma artesanal, lo cual requiere mucho tiempo
y es parcialmente redundante.
Esto se debe principalmente a que el algoritmo de VisualTopo para estimar los contornos es simple y tampoco lo entendemos
muy bien.
Parece ser que utiliza las medidas de izquierda, derecha, arriba, abajo en cada vértice para trazar un contorno formado
por prismas rectangulares.

El proyecto `.tro` incluye columnas adicionales en los vértices, para representar esas dimensiones, además del desarrollo
acumulado del recorrido (la suma de las distancias entre vértices).
No tenemos claro cómo calcula esas dimensiones.
Como las medidas adicionales no las podemos tomar en posiciones perfectamente ortogonales, es posible que VisualTopo
realice interpolaciones para estimar las medidas ortogonales en los vértices, y usar esas en la visualización del
contorno en 3D.
Actualmente, sobreescribimos esas dimensiones manualmente.

Sería interesante explorar alternativas desatendidas para generar las poligonales y los contornos aproximados
directamente a partir de los ficheros exportados de TopoDroid (e integrados), sin necesidad de intervención manual
adicional.
Idealmente, la herramienta utilizaría todas las medidas adicionales y auxiliares (cuantas más mejor) para reconstruir el
contorno como una superficie definida por una nube de puntos, en vez un conjunto de prismas.
Al mismo tiempo, permitiría unir ficheros de varias sesiones sin necesidad de unirlos manualmente.

Por otro lado, sería también interesante explorar alternativas de visualización que permitan superponer los bocetos
en el contorno estimado y la poligonal, de forma que pueda realizarse el dibujo vectorial sin necesidad de dibujarlo en
papel primero.
Sin necesidad de cambiar AutoCAD (para mantener las librerías de símbolos), se debería poder exportar la proyección en
planta de la poligonal, las medidas auxiliares y el contorno aproximado en un fichero con capas que pueda abrirse en
AutoCAD.

```{note}
Como AutoCAD es una herramienta de pago, sería interesante probar si alguna alternativa libre puede leer los símbolos y
si permite cargar imágenes como referencia para dibujar sobre ellas.
```

Therion parece una solución posible para realizar estas mejoras en el flujo de trabajo.
Sin embargo, puede que el flujo de trabajo principal al que esté orientado no sea este exactamente.
Es decir, algunas piezas/partes de Therion pueden simplificar nuestro flujo, sin necesidad de cambiar la herramienta
principal de dibujo vectorial.

## Gestión de datos

Queremos disponer de copia de todos los pasos del proceso, por si cometiéramos algún error y necesitáramos volver un
paso atrás.

- Hoja(s) escaneada(s) con el boceto y las anotaciones de cada sesión.
- Sesión exportada en crudo de {doc}`TED:sw/topodroid` en formato `.zip`, tras cada jornada.
  - Sesión revisada exportada de {doc}`TED:sw/topodroid` en varios formatos, tras cotejar el boceto y las notas en papel:
    - `.zip`.
    - `.tro`/`.trox` (para su uso en {doc}`TED:sw/visualtopo`)
    - `.th` (para su uso en {doc}`TED:sw/therion`).
- Integración incremental de todas las sesiones.
