# Casco

El producto de referencia en la espeleología a nivel mundial es el ECRIN ROC de [Petzl](https://www.petzl.com).

Fue introducido en el mercado a primeros de los noventa y durante dos décadas fue la opción más usada.
Con 455 gramos de peso, fue ligero cuando se introdujo, aunque hoy en día es habitual encontrar modelos de 350 gramos.
Se fabricaba en talla única, ajustable de 53 a 63 centímetros de circunferencia.
Con catorce orificios, permitía una adecuada ventilación en entornos cerrados o en verano.
Disponía de cuatro clips de plástico para sujección de linternas frontales con cinta elástica.
Al ser una carcasa de policarbonato sin espuma, resultaba sencillo realizar orificios para instalar linternas frontales
más pesadas/grandes, como las utilizadas en espeleología en comparación con alpinismo o escalada.
Además, como la carcasa estaba suspendida, había espacio para guardar algunos elementos blandos como la manta térmica.
Lamentablemente, Petzl decidió descatalogar el modelo en 2012 y, para consternación de la comunidad espeleológica, no
parece que tengan intención de ofrecer un diseño equivalente.
Puesto que sólo puede adquirise de segunda mano, el precio se va encareciendo notablemente.
Aún así, y a pesar de ser pesado para los estándares actuales, es todavía una opción muy solicitada.

Lo más similar que puede obtenerse hoy en día en el catálogo de Petzl son los modelos [VERTEX](https://www.petzl.com/ES/es/Profesional/Cascos/VERTEX)/[VERTEX VENT](https://www.petzl.com/ES/es/Profesional/Cascos/VERTEX-VENT) de su línea
profesional.
Con un peso ligeramente mayor (490 gramos), también es una carcasa suspendida, de talla única 53-63 cm, y tiene soportes
para linternas frontales con cinta elástica.

En la línea deportiva, el único modelo indicado para espeleología es el [BOREO CAVING](https://www.petzl.com/ES/es/Sport/Cascos/BOREO-CAVING).
Se ofrece en talla pequeña (48-58 cm), además de la grande de 53-61 cm, ya que la carcasa tiene espuma y no está
suspendida.
Incluye pletinas delantera y trasera para fijar una lintera DUO (modelo RL, S o Z2) del mismo fabricante.
Por lo tanto, salvo que se quiera usar una de esas linternas y esperar que Petzl no cambie el tipo de anclaje, no es la
opción más atractiva.

El modelo de la línea profesional [STRATO](https://www.petzl.com/ES/es/Profesional/Cascos/STRATO)/[STRATO VENT](https://www.petzl.com/ES/es/Profesional/Cascos/STRATO-VENT) (hasta el año 2018, existía el modelo ALVEO, que fue sustituido por STRATO;
también se rediseñó el VERTEX), parece ser un término medio entre el VERTEX y el BOREO CAVING.
El STRATO tiene una carcasa con espuma, pero no incluye pletinas para linternas, y sólo está disponible en talla 53-63 cm.
Sin embargo, es difícil discernir entre el STRATO y el [BOREO](https://www.petzl.com/ES/es/Sport/Cascos/BOREO) simple.

El modelo BOREO está disponible por 50-60€, el BOREO CAVING por 70-75€, el VERTEX por 75-85€ y el STRATO por 90-100€.

Sea como fuere, para espeleología podemos usar cualquier casco de escalada/alpinismo con carcasa exterior rígida en el
que podamos sostener o atornillar una linterna frontal y preferiblemente una auxiliar.

Por ejemplo, el Camp [TITAN](https://www.camp.it/d/ot/en/outdoor/product/1804), disponible por 40-45€, es una carcasa
suspendida, de tallas 48-56 cm (385 gramos) o 54-62 cm (435 gramos), tiene orificios de ventilación y cuatro clips para
sujección de linternas frontales con cinta elástica.
Por un precio similar al BOREO simple, es más parecido al ECRIN ROC y cuesta la mitad que el VERTEX.
