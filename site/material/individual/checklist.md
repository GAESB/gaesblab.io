# Checklist

Además de la lista completa del equipo individual, antes de cada actividad se deben revisar los siguientes:

- Pilas de repuesto cargadas.
- Comida y agua para fuera.
- Comida y agua para dentro.
- Calcetines, camiseta, sudadera y chaqueta secos.
  - Gorro, guantes, buff, chubasquero.
