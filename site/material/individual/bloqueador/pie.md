# Bloqueador de pie

El producto de referencia en el grupo por su fiabilidad y relación calidad-precio es el modelo [PANTIN](https://www.petzl.com/ES/es/Sport/Bloqueadores/PANTIN)
de [Petzl](https://www.petzl.com).

A diferencia de los bloqueadores ventral o de mano, que son elementos de seguridad necesarios para la progresión, el uso
de un bloqueador de pie es opcional.
Su función principal no es soportar todo el peso, sino ayudar en el direccionamiento y arrastre de la cuerda, además de
mantener una posición más vertical.
No obstante, tal como se explica en la nota técnica de Petzl, permite progresar usando alternativamente el pedal y el
bloqueador de pie, de forma que no es necesario transferir todo el peso al bloqueador ventral para mover el puño.

Hay alternativas de algunos fabricantes como Edelrid CRUISER, Climbing Technology QUICK STEP o Camp Turbofoot, pero
todos son notablemente más caros que el PANTIN de Petzl y no tenemos constancia de que ofrezcan ninguna mejora
significativa.

No obstante, al no ser parte del equipo de protección individual, puede ser interesante probar algún modelo de origen
asiático, ya que el precio es de 15-30€ (por ejemplo, [aliexpress.com/item/1005006127843234](https://es.aliexpress.com/item/1005006127843234.html)),
mientras que los occidentales cuestan más de 40€.
Es de esperar que la diferencia se traduzca en la fricción requerida para el paso de la cuerda, la dureza y desgaste de
los materiales (y por ello la capacidad de retención).
Sin embargo, no debería suponer un problema para las cuerdas.

Con respecto al uso de bloqueador de pie derecho o izquierdo, depende de gustos y/o de cuál sea la pierna fuerte.
Sólo cambia ligeramente la manera de enfrentarse a los fraccionamientos, pero una vez adquirida la costumbre no es
relevante.
Si se utiliza sólo un pie en el pedal, el bloqueador irá en el pie opuesto.
Por lo tanto, asumiendo que la pierna fuerte sea la derecha, se recomienda usar el bloqueador de pie en la izquierda.
Sin embargo, originalmente Petzl sólo fabricaba el PANTIN derecho, por lo que hay miembros del grupo que están
acostumbrados a utilizar el pedal en la pierna izquierda.
