# Bloqueador ventral

El producto de referencia en el grupo por su fiabilidad y relación calidad-precio es el modelo [CROLL S](https://www.petzl.com/ES/es/Sport/Bloqueadores/CROLL-S) de [Petzl](https://www.petzl.com).

Existe también el modelo [CROLL L](https://www.petzl.com/ES/es/Sport/Bloqueadores/CROLL-L) que permite su uso con cuerdas
de hasta 13 mm en vez de 11 mm.
Es notablemente más voluminoso y en la actividad del grupo nunca se utilizan cuerdas de más de 11 mm, por lo que es
innecesario y puede resultar molesto en pasos estrechos.

Hay productos muy similares de otros fabricantes (Fixe Dome, Fixe Indu, Singingrock Cam Clean, Rock Empire Chest Up,
Camp Turbochest, Climbing Technology CHEST ASCENDER+), pero todos a excepción del Fixe Dome tienen un precio superior al Petzl Croll S y parecen más pensados
para su uso en ambientes sin tanto barro (alpinismo o trabajos verticales).

```{note}
Nótese que el origen de Petzl es la espeleología, aunque hoy en día sea muy conocida por sus productos de escalada.
Por lo que no es de extrañar que dominen el mercado en productos tan específicos.
```
