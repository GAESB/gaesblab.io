# Bloqueador de mano

El producto de referencia en el grupo por su fiabilidad y relación calidad-precio es el modelo [BASIC](https://www.petzl.com/ES/es/Sport/Bloqueadores/BASIC)
de [Petzl](https://www.petzl.com).

```{note}
Nos referimos habitualmente al bloqueador de mano como *puño*.
```

No hay muchas alternativas equivalentes al Petzl BASIC salvo [ASCENDER SIMPLE+](https://www.climbingtechnology.com/en/outdoor-en/pulleys-and-ascenders/ascenders/ascender-simple-plus)
de [Climbing Technology](https://www.climbingtechnology.com).
Sin embargo, el mecanismo es exactamente el mismo que el de los comunmente llamados *ascendedor yumar*, con la diferencia
de que éstos son más voluminosos por incluir empuñadura.
Si disponemos de un yumar de cualquier fabricante (Altus, Fixe, Petzl, Edelrid, Grivel, Kong, Singing Rock, Climbing
Technology), sea para derecha o izquierda, podemos usarlos en vez de adquirir un Petzl BASIC.
Deberemos adaptar para ello la longitud del pedal.
No obstante, es recomendable adquirir un Petzl BASIC, ya que en pasos estrechos la voluminosidad del *yumar* puede ser
muy molesta en comparación.
Asimismo, de forma similar a los bloqueadores ventrales, el Petzl BASIC parece mejor diseñado para evitar la acumulación
de barro en el gatillo.

Existe también un tipo de bloqueador que no utiliza palanca sino un mosquetón para ejercer presión sobre la cuerda.
Por ejemplo, el [TIBLOC](https://www.petzl.com/ES/es/Sport/Bloqueadores/TIBLOC) de Petzl.
Estos no son recomendables para un uso prolongado ya que el desgaste es notablemente mayor y también el esfuerzo.
