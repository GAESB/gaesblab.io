# Descensor

El producto de referencia en el grupo por su fiabilidad y relación calidad-precio es el modelo [STOP](https://www.petzl.com/ES/es/Sport/Aseguradores--descensores/STOP)
de [Petzl](https://www.petzl.com), unido al mosquetón semicircular mediante un mosquetón de seguridad (preferiblemente
con rosca hexagonal) y usado junto con un mosquetón de freno de acero.

El modelo actual de STOP es esencialmente el [GRIGRI](https://www.petzl.com/ES/es/Sport/Aseguradores--descensores/GRIGRI)
clásico en una carcasa diferente que permite forzar un giro más de la cuerda.
Existe un modelo sin función autoblocante (en términos de Petzl *"frenado asistido"*), el [SIMPLE](https://www.petzl.com/ES/es/Sport/Aseguradores--descensores/SIMPLE), que son "sólo" dos poleas en la carcasa del STOP.
El SIMPLE permite sustituir las poleas, y se venden recambios por separado.
Los modelos anteriores de STOP eran más similares al SIMPLE y también se podían sustituir las poleas.
El actual no lo permite.
Por lo tanto, una razón para considerar el uso del Petzl SIMPLE en vez del STOP puede ser que cuesta la mitad y permite
cambiar las poleas por 20-25€ en vez de tener que comprar un producto nuevo.
Por otro lado, el STOP al tener el mismo mecanismo que el GRIGRI puede usarse para asegurar al primero en escaladas,
caso que se da a menudo en exploraciones.

Puede oirse de alguna fuente también que es recomendable el uso del SIMPLE inicialmente para no crear dependencia de la
función autoblocante, por si en alguna situación no pudiéramos disponer de la misma.
Es decir, para aprender "mejor" cómo bloquear el descensor, sin necesidad de que lo haga por si solo (aunque sea sólo
parcial).
Sin embargo, en años recientes ha habido accidentes que podrían haberse evitado si se hubiera utilizado STOP en vez de
SIMPLE.
Por eso, en el grupo se recomienda el uso de STOP desde el principio.

Aunque existen alternativas de Climbing Technology, Kong y Singing Rock, todas son notablemente más caras que los
productos de Petzl y no ofrecen ventajas significativas.
