# Cabos

La solución recomendada es el uso de dos cabos asimétricos (de aproximadamente 35 cm y 65 cm) con cuerda dinámica de
9 mm.
Con 3 m de cuerda pueden hacerse ambos cabos (1.4 m + 1.6 m, reservando 50 cm para cada nudo).

```{note}
Hay escuelas que recomiendan el uso de tres cabos de anclaje, de forma que se disponga de un cabo largo para pasamanos y
otras maniobras independiente del puño.
Sin embargo, en el grupo se realizan todas las maniobras necesitando sólo dos cabos y utilizando un mosquetón auxiliar
para colgar el puño en el arnés cuando no se esté usando.
```

Algunas prefieren hacerlo con un solo cabo que tenga dos nudos en los extremos y otro en la zona central, mientras que
otras prefieren dos cabos independientes.
La ventaja de un solo cabo es que sólo hay una coca que pasar por el mosquetón semicircular; mientras que la desventaja
es que si esa coca se estropea, pueden fallar ambos cabos.
Con respecto a usar dos cabos independientes las ventajas son que pueden ser de colores diferentes y que uno se estropee
no afecta al otro; mientras que las desventajas son que hay dos cocas que pasar por el mosquetón central y es un nudo más
que vigilar.

No es recomendable el uso de cabos de cinta como Petzl [SPELEGYCA](https://www.petzl.com/ES/es/Sport/Elementos-de-amarre/SPELEGYCA)
o Rock Empire Ypsilon, ya que hoy en día sabemos que la cuerda dinámica absorve más adecuadamente caídas de hasta
factor 2.
Aunque en espeleología rara vez sucede, en algún pasamanos o cabecera de pozo puede darse un resbalón y es preferible
el margen que da el dinamismo de la cuerda.

Tampoco son recomendables los modelos siméticros como Petzl [DUAL CANYON CLUB](https://www.petzl.com/ES/es/Sport/Elementos-de-amarre/DUAL-CANYON-CLUB),
Beal [DYNAPARK](https://www.beal-planet.com/en/longes-sport/1562-dynapark-80cm-x-80cm.html),
o Camp Basic, porque en espeleología es útil la asimetría de los cabos, tanto en pasamanos como al
pasar fraccionamientos o nudos en la progresión vertical.

Los modelos que permiten ajustar la asimetría y/o regular uno de los cabos (el corto o el largo), como
Petzl [DUAL CANYON GUIDE](https://www.petzl.com/ES/es/Sport/Elementos-de-amarre/DUAL-CANYON-GUIDE),
Petzl [DUAL CONNECT ADJUST](https://www.petzl.com/ES/es/Sport/Elementos-de-amarre/DUAL-CONNECT-ADJUST),
Petzl [DUAL CONNECT VARIO](https://www.petzl.com/ES/es/Sport/Elementos-de-amarre/DUAL-CONNECT-VARIO),
Rock Empire [STAND Y](https://www.rockempire.cz/en/product/stand-y),
o Rock Empire [STAND V](https://www.rockempire.cz/en/product/stand-v),
pueden usarse si no tenemos una solución más adecuada.
Sin embargo, es muy probable que el exceso de cuerda y voluminosidad nos resulten molestas.

Las soluciones comerciales recomendables son
Rock Empire [Lanyard Y](https://www.rockempire.cz/en/product/lanyard-y) (de 35/65 cm, y 11 mm),
Fixe [Manyac](https://www.fixeclimbing.com/es/cintas/438-manyac-8436020414039.html) (de 40/70 cm, y 9 mm),
o Beal [DYNADOUBLECLIP](https://www.beal-planet.com/en/longes-sport/1561-dynadoubleclip.html).

Sin embargo, 3 m de cuerda de 9 mm Fixe Monkey cuestan 7.5-11€; la mitad que las soluciones comerciales.
Por eso, unido a la posibilidad de ajustar la longitud de los cabos a la persona antes de tensarlos por completo,
la solución recomendada es adquirir 3 m de cuerda.
Nótese que a diferencia de la escalada deportiva o el alpinismo, en espeleología los cabos son elementos usados
continuamente en la progresión; no sólo para descansar/reunirse.

Se utilizan nudos de medio pescador triple en los extremos de los mosquetones de anclaje, y un nudo simple en el extremo
del mosquetón semicircular.

```{attention}
En caso de usar un solo cabo, el nudo central no puede ser un nudo de alondra, porque puede deslizar.
Debe ser un nudo simple.
Los nudos de ocho y nueve son menos compactos y necesitan más uso para aprentarse definitivamente.
```

```{attention}
Se requieren 1.10-1.20 m para hacer un cabo corto de aproximadamente 35 cm, usando un medio pescador y un simple.
¡No es suficiente con 1 m para un cabo corto!
```
