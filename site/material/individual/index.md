# Equipo individual

La adquisición del equipo individual completo para la práctica de la espeleología reviste cierta complejidad ya que, a
diferencia de otras actividades de aventura o montaña, los componentes fundamentales y el número de ellos utilizados por
nóveles no difieren significativamente de los utilizados por las más experimentadas.

El número de componentes necesarios (más de una veintena) hacen que el desembolso inicial no sea despreciable, aunque
tampoco excesivo.
La inversión para disponer de un equipo individual completo (arneses, bloqueadores, descendedores, pedal, cabos,
iluminación, ropa, sacas y botiquín) es de entre 600€ y 1000€.

En esta sección se explica qué productos o soluciones se utilizan habitualmente para cada componente, teniendo en
consideración la progresión por una sola cuerda de entre 8 y 11 mm, y la utilización de dos cabos de anclaje (uno de
ellos usado principalmente unido al bloqueador de mano).

- Arneses
  - {doc}`Pélvico <arnes/pelvico>` |65-75€|
  - {doc}`Pecho <arnes/pecho>` |10-35€|
- Bloqueadores
  - {doc}`Ventral <bloqueador/ventral>` |35-45€|
  - {doc}`Mano <bloqueador/mano>` |35-45€|
  - {doc}`Pie <bloqueador/pie>` |45-55€|
- {doc}`descensor` |90-110€|
- {doc}`pedal` |12-25€|
- {doc}`cabos` |8.5-12€|
- {doc}`mosqueton/index`
  - {doc}`Semicircular <mosqueton/semicircular>` |8.5-25€|
  - {doc}`Seguridad <mosqueton/seguridad>` (descensor) |10-14€|
  - {doc}`Freno <mosqueton/freno>` |10-22€|
  - {doc}`Cabo largo <mosqueton/largo>` |8-14€|
  - {doc}`Cabo corto <mosqueton/corto>` |8-10€|
  - {doc}`Pedal <mosqueton/pedal>` |2-10€|
  - {doc}`Auxiliar <mosqueton/auxiliar>` |2-10€|
- {doc}`casco`
- {doc}`linterna/index`
  - {doc}`Frontal <linterna/frontal>`
  - {doc}`Mano <linterna/mano>`
- {doc}`saca/index`
  - {doc}`Cintura <saca/cintura>`
  - {doc}`Espalda <saca/espalda>`
- Ropa exterior
  - {doc}`Mono/buzo <exterior/mono>`
  - Botas
  - Guantes
  - {doc}`exterior/rodilleras`
- Ropa interior
- Botiquín
  - Cantimplora
  - Navaja
  - Manta
  - Alcohol en gel
  - Bolsa impermeable
    - Mechero

```{toctree}
:hidden:
arnes/index
bloqueador/index
descensor
pedal
cabos
mosqueton/index
casco
linterna/index
saca/index
exterior/index
checklist
```

## Referencias

- [Sergio García-Dils de la Vega, Director de la Escuela Española de Espeleología. El equipo personal de progresión
  vertical del espeleólogo. SUBTERRÁNEA 29.](https://www.efiemer.com/wp-content/uploads/2019/02/equipo_personal_de_progresion_vertical.pdf)
- [Derek Bristol. Caving Gear. derekbristol.com/gear-overview](https://www.derekbristol.com/gear-overview)
