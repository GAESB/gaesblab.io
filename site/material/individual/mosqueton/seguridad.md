# Mosquetón de seguridad

El mosquetón de seguridad se utiliza para conectar el descensor al mosquetón central (semicircular).
Se recomienda que sea simétrico para un mejor reparto de la carga, ya que sólo va a alojar un elemento, por lo que el
espacio adicional de geometrías tipo HMS no es necesario.

Sorprendentemente, el único modelo simétrico con rosca hexagonal parece ser el [Fixe Symmetric Screw](https://www.fixeclimbing.com/en/inicio/1195-419-symmetric-screw-8436020410475.html#/4-packing_unit-1/198-size_long-110mm/235-material-aluminium_7075).
El hecho de que disponga de rosca hexagonal es interesante porque el barro/arenilla tienden a atascar los mecanismos.
La forma hexagonal permite usar una llave.

Los riesgos principales asociados a este mosquetón son:

- Que la rosca se suelte por el rozamiento con la cuerda, o al pasar por gateras/laminadores, y después el mosquetón se
  abra accidentalmente.
  Es muy importante revisar la rosca antes de iniciar cada descensor.
- La carga transversal, al mover/recolocar el descensor en el paso de fraccionamientos o nudos.
  Es muy importante revisar la posición del mosquetón antes de transferir la carga al descensor.

Por estas razones, puede ser interesante utilizar mosquetón simétrico con cierre automático (que no se atasque con el
barro/arenilla) y que disponga de algún mecanismo que fuerce la posición del mosquetón.

El Petzl [OK](https://www.petzl.com/ES/es/Profesional/Conectores/OK) está disponible con cierre TRIACT-LOCK, SCREW-LOCK
y BALL-LOCK, y es compatible con la barra [CAPTIV](https://www.petzl.com/ES/es/Profesional/Conectores/CAPTIV).
El cierre TRIACT-LOCK es el mismo que el del OMNI automático.
Sin embargo, cuesta un 50% más que el Fixe Symmetric Screw, y las barras CAPTIV sólo se venden en packs de 10 unidades a
17.5€.
Además, en [Elección del mosquetón para conectar al arnés un descensor con gatillo I'D S, RIG, STOP... ](https://www.petzl.com/ES/es/Profesional/Eleccion-del-mosqueton-para-conectar-al-arnes-un-descensor-con-gatillo-I-D-S--RIG--STOP---?ProductName=i-d-s)
Petzl recomienda el cierre SCREW-LOCK por *"Fiabilidad en ambiente sucio (barro, arena...)"*.
No parece una alternativa cierta que anime al desembolso.

```{important}
Si no existe una solución comercial antigiro para nuestro mosquetón, puede ser interesante utilizar una goma o pieza
de plástico.
```

Petzl dispone del modelo [FREINO](https://www.petzl.com/INT/es/Sport/Mosquetones-y-cintas-expres/FREINO) que resulta
atractivo porque tiene bloqueo automático e incluye un gatillo de frenado que podría sustituir el mosquetón de frenado.
Sin embargo, al estar fabricado en aluminio, el desgaste es notable en su uso como freno, y una vez estropeada esa parte
pierde su atractivo.
Además, en ciertas maniobras el mosquetón de freno se utiliza como un punto más de seguridad (siempre auxiliar), y el
gatillo del FREINO no parece suficiente para soportar una caida.
Por ello, se recomienda el uso de un mosquetón de freno independiente.
