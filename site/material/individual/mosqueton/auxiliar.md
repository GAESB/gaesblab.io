# Mosquetón auxiliar

Además de los mosquetones necesarios para la progresión, son necesarios hasta media docena de mosquetones auxiliares, de
los que no depende directamente la vida, pero si un avance cómoda/adecuado.

- Dependiendo del tipo de arnés de pecho escogido, puede ser necesario un mosquetón auxiliar para unirlo al bloqueador
  ventral o para cerrar el pecho.
- Cuando se esté usando el cabo largo en pasamanos, puede ser conveniente disponer de un mosquetón para llevar
  el bloqueador de mano en las cintas portamaterial.
- Otro material que habitualmente querremos tener temporalmente en las cintas portamaterial serán llaves fijas, medidor,
  taladro, etc.
- Las sacas las llevaremos en el mosquetón central (semicircular) o en anillos/aros de cordino de Dyneema en
  en la cintura del arnés pélvico.

Al igual que en el caso del mosquetón para el pedal, puede haber cierta flexibilidad en la elección de estos mosquetones,
pero no deben ser tan pequeños que se dificulte su utilización con guantes.
Siempre es recomendable llevar uno o dos mosquetones de más que soporten 22 kN y tengan al menos 90 mm, para que puedan
usarse en maniobras de rescate y no sólo para transporte.

```{important}
Para transportar el material valioso, como la saca personal, querremos mosquetones con rosca.
```
