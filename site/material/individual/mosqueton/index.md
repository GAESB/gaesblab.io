# Mosquetones

Para la progresión en espeleología se utilizan al menos media docena de mosquetones, cada uno con un propósito y
características particulares.
Aunque cualquier mosquetón con la resistencia suficiente podría servir en situación de fortuna, es importante escoger
adecuadamente cada uno para mejorar la seguridad y evitar complicaciones en las maniobras.

```{toctree}
semicircular
seguridad
freno
corto
largo
pedal
auxiliar
```
