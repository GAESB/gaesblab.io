# Mosquetón de freno

Como la designación indica, el mosquetón de freno se utiliza para frenar el descensor o para un destrepe de fortuna.
Es el mosquetón que más rozamiento sufre, ya que su uso principal es precisamente generar fricción con la cuerda.
Por ello, debe ser de acero, no de aluminio.

```{note}
Aunque en algunas maniobras puntuales puede utilizarse el mosquetón de freno como un punto auxiliar más de seguridad,
no es necesario ni recomendable que tenga cierre de seguridad.
No tiene capacidad de retención de por si, y en el paso de fraccionamientos y nudos el cierre sólo entorpecería las
maniobras.
```

El producto de referencia en el grupo es el modelo [Handy](https://raumerclimbing.com/es/productos/classic/mosquetones-inox/handy-mosqueton-de-freno-inox/)
de [Raumer](https://raumerclimbing.com).
Se trata de un diseño muy específico pensado para su uso en espeleología y para ser conectado al mosquetón central
directamente.
Es muy efectivo sobre cualquier tipo de cuerda, incluso con diámetros pequeños o con cuerdas mojadas.

Sin embargo, cuesta aproximadamente 22€, por lo que pueden valorarse otras alternativas si se desea reducir el
desembolso inicial.
Entre los mosquetones de acero sin seguro, deberá ser un modelo suficientemente pequeño para que el descensor no pueda
introducirse accidentalmente.
Por ejemplo, el [AISI 316 Straight Gate](https://www.kong.it/en/product/mini-d-inox/) de Kong, con unas dimensiones de
80 x 45 mm es una opción interesante por 9-10€.

```{note}
En caso de usar un mosquetón de freno diferente del Handy, hay quien prefiere conectarlo al mosquetón de seguridad,
junto al descensor, en vez del mosquetón central.
```
