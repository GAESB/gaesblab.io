# Mosquetón para pedal

Como la designación indica, el mosquetón para el pedal se utiliza para unir el pedal al bloqueador de mano, a cabeceras
o fraccionamientos.
Se trata de un mosquetón auxiliar, por lo que a diferencia de los demás, de este no depende la vida.
Resulta necesario para la progresión ascendente, y puede ser necesario para el paso de fraccionamientos o nudos durante
el descenso.
Por lo tanto es importante disponer de un mosquetón para el pedal, pero no tiene requerimientos particulares.
Se recomienda usar uno sencillo, de gatillo recto y de forma similar al del cabo corto.

Al no ser parte del equipo de protección individual, pueden ser interesantes modelos de origen asiático.
Por 2-2.5 € los hay de 80 x 50 mm, 22 g, y apertura de al menos 20 mm que dicen ser de 12 kN.
El diseño es similar a los Nano 22 de Camp, pero estos no pretenden ser aptos para escalada.

Aunque depende de gustos, disponer de un mosquetón notablemente más pequeño que los de los cabos ayuda a identificar
visualmente cuál es el pedal si se necesita moverlo del bloqueador de mano a un fraccionamiento o cabecera.
Sin embargo, no debe ser tan pequeño que se dificulte la maniobrabilidad con guantes, o no entre en las instalaciones.
