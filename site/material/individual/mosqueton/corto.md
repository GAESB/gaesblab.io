# Mosquetón para cabo corto

El mosquetón del cabo corto se utiliza para, como la designación indica, unir el cabo corto a pasamanos,
cabeceras, fraccionamientos o al bloqueador de mano en el paso de nudos.
Se recomienda una geometría que facilite su uso en cuerdas e instalaciones, y con leva recta para reducir el riesgo de
apertura accidental con cuerdas tensas o rígidas.
A diferencia de la escalada deportiva, en espeleología no es necesario introducir el mosquetón en posiciones tan
comprometidas que la curvatura de la leva favorezca significativamente la maniobra;
sin embargo, sí se dan muchas situaciones en pasamanos donde la curvatura puede ser peligrosa.
Asimismo, es importante que la geometría del cierre sea antienganche.

Como es el tipo de mosquetón usado para cintas exprés y cabos de anclaje en otras disciplinas, hay una gran variedad.
Los Camp [Nano 22](https://www.camp.it/d/us/us/outdoor/product/1475), por menos de 8€, son interesantes porque a pesar
de su reducido tamaño (86 x 52 mm, 22 g) soportan 21 kN en su eje longitudinal y 8 kN en su eje transversal.
El único pero es la apertura de 17 mm.
Entre 8-10€ hay varios de tamaño y peso similar:

- Camp [Orbit Straight Gate](https://www.camp.it/d/us/us/outdoor/product/78), 98 x 58 mm, 42 g, 24 kN y 7 kN, 20 mm.
- Camp [Photon Straight Gate](https://www.camp.it/d/us/us/outdoor/product/74), 100 x 60 mm, 36 g, 22 kN y 7 kN, 21 mm.
- Wild Country Session Straight, 41 g, 23 kN y 8 kN, 20 mm.
- Petzl [Djinn](https://www.petzl.com/ES/es/Sport/Mosquetones-y-cintas-expres/DJINN), 100 x 62 mm, 45 g, 23 kN y 8 kN,
  24 mm.
- Petzl [Spirit](https://www.petzl.com/ES/es/Sport/Mosquetones-y-cintas-expres/SPIRIT), 94 x 57 mm, 37 g, 23 kN y 7 kN,
  21 mm.

De entre los anteriores, el Petzl Spirit ha sido muy popular por ser el más compacto y con buena apertura.
Sin embargo, en el último rediseño han añadido orificios al cuerpo para reducir el peso, para su uso en escalada
deportiva.
Esos orificios tienden a hacer que trozos de barro se queden pegados al mosquetón.
Por eso, se recomiendan los modelos de Camp o Wild Country si se quiere lo más parecido al Spirit, o el Petzl Djinn si
se prefiere una apertura ligeramente mayor.
