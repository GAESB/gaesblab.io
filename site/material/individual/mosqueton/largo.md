# Mosquetón para cabo largo

El mosquetón del cabo largo se utiliza para, como la designación indica, unir el cabo al bloqueador de mano, a pasamanos
o a nudos.
Como en el caso del mosquetón del cabo corto, se recomienda una geometría que facilite su uso en pasamanos/nudos/combas,
con leva recta y cierre antienganche.

En comparación con el cabo corto, durante la progresión habitual el largo rara vez se tensiona con todo el peso del cuerpo.
Es un punto de seguridad adicional por si fallaran los principales (el bloqueador de ventral o el cabo corto, en función
de la situación).
Por lo tanto, en caso de que el cabo largo tuviera que ser tensionado por completo para detener una caída, es de esperar
que el mosquetón reciba un latigazo.
Es por ello discutible la preferencia por un mosquetón con o sin cierre de seguridad.
En los pasamanos, el cierre de seguridad resulta poco útil porque ambos cabos suele estar razonablemente tensionados, y
hay que cambiarlos de tramo constantemente.
Sin embargo, en los pasos de fraccionamientos/nudos, sea durante el ascenso o el descenso, el cierre de seguridad parece
deseable.

El producto de referencia en el grupo es el modelo [Sigma K8G](https://grivel.com/es/products/sigma-k8g)
de [Grivel](https://grivel.com).
El cierre es de tipo [Twin-Gate](https://grivel.com/es/blogs/news/revolution-twin-gate-carabiner) que, como el nombre
indica, consiste en dos compuertas en oposición (una de tipo alambre hacia fuera y otra hacia dentro).
Una vez entendido qué puerta abrir primero para realizar las maniobras de conexión/desconexión, se emplea el mismo
tiempo que con un mosquetón sencillo; y, aunque no protege de todas las aperturas accidentales, es más seguro.

Sin embargo, puesto que cuesta 13.5€, se pueden valorar otras opciones.
Cualquiera de los modelos utilizados para el cabo corto puede usarse también para el largo.

```{note}
En el bloqueador de mano se unen el mosquetón del cabo largo y el mosquetón del pedal.
Es recomendable unir sólo un mosquetón de seguridad al bloqueador de mano y unir tanto el cabo largo como el mosquetón
del pedal al mosquetón de seguridad.
Aunque durante las técnicas de progresión habituales ese mosquetón adicional no es necesario, puede resultar útil en
maniobras de autorrescate.
El orificio del bloqueador de mano tiene aristas que no lo hacen compatible con cuerdas directamente (para realizar un
contrapeso, por ejemplo), mientras que un mosquetón tiene una superficie curva que no daña las cuerdas, y al mismo
tiempo no se ve dañado por el orificio del bloqueador.
Asimismo, el mosquetón adicional es útil para colgar el bloqueador de mano del portamateriales cuando estemos usando
el cabo largo en un pasamanos.
```
