# Mosquetón semicircular

El mosquetón semicircular es, junto con el de freno, el más característico de los mosquetones de espeleología a ojos de
los practicantes de escalada deportiva o alpinismo.
Se trata del mosquetón central, que ejerce de cierre de cintura del arnés pélvico, y al que se conectan todos los
elementos.

El producto de referencia en el grupo es el modelo [OMNI](https://www.petzl.com/ES/es/Sport/Mosquetones-y-cintas-expres/OMNI)
de [Petzl](https://www.petzl.com), en su versión con bloqueo automático TRIACT-LOCK.
También está disponible con bloqueo manual SCREW-LOCK.
Se recomienda la versión con bloqueo automático porque el barro/arenilla tienden a atascar la rosca y porque en el paso
por gateras o laminadores puede soltarse la rosca.
No es habitual, pero tampoco extraño.

Existen modelos de otras marcas similares al Petzl OMNI, como por ejemplo el Camp Triad (Lock o 3Lock), pero son
más caros y no tenemos constancia de que ofrezcan ninguna mejora significativa.
En este producto, como en los bloqueadores, se aprecia que el origen de la marca Petzl está ligado a la espeleología y
domina el mercado en lo que respecta a los productos más específicos.

El Petzl OMNI TRIACT-LOCK cuesta aproximadamente 25€, por lo que es el más caro de los mosquetones.
En caso de no querer/poder hacer un desembolso inicial notable, también hay maillones semicirculares por 8.5-15€.
La desventaja de los maillones es una resistencia ligeramente menor (13 kN frente a 15 kN) y menor comodidad a la hora
de abrirlo/cerrarlo.
Aunque en principio sólo hay que abrirlo/cerrarlo al ponerse/quitarse el arnés.
Es, de hecho, el único de los mosquetones en el que no importa que se use un maillón porque en maniobras o autosocorro
no es necesario soltarlo.
