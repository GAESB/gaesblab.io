# Saca de cintura

La saca de cintura, de aproximadamente 5-8L, es donde se transporta el material personal (pilas de repuesto, botiquin,
prenda de abrigo, etc.).
Debe ser pequeña para no obstaculizar la progresión, ya que normalmente se llevará otra saca colectiva.
De hecho, si cabe, introducimos la saca personal en la colectiva para reducir el número de bultos.

El producto de referencia en el grupo por su relación calidad-precio es el modelo [Personal 5](https://landjoff.com/product/1/personal-5.html)
de [Landjoff](https://landjoff.com),
que está disponible en media docena de colores por 11.5€.
Por apenas dos euros más está el modelo [Mini 5](https://landjoff.com/product/10/mini-5.html) (también en versión de 3L [Mini 3](https://landjoff.com/product/9/mini-3.html));
este tiene un cierre diferente que no es recomendable porque la tela tiene peor vejez que el PVC y cuando se estropea no
se puede cerrar la saca correctamente.

Petzl no tiene sacas de cintura.
El modelo más pequeño es el [PERSONNEL 15](https://www.petzl.com/ES/es/Sport/Sacos-y-accesorios/PERSONNEL-15L), que es
de espalda.

[Material Técnico de Espeleología (MTDE)](https://www.mtde.net) tiene el modelo [Minikit](https://www.mtde.net/blog/2010/06/04/minikit-2/),
pero siendo ligeramente más cara que la Personal 5 de Landjoff, pesa más y la capacidad, 3.5L, es muy justa.
El modelo [Bocata](https://www.mtde.net/blog/2010/06/04/bocata-2/) tiene sólo 1L de capacidad.

[Aventure Verticale (AV)](https://www.aventureverticale.com) tiene varios modelos:

- [Flat waist bag +skirt AVSP22P](https://www.aventureverticale.com/en/caving/waist-bag-avsp22p.html) 5-7L oval 8 x 18 x 34 cm 33€
- [Round waist bag +skirt AVSP22R](https://www.aventureverticale.com/en/caving/waist-bag-avsp22r.html) 5-7L oval 11 x 15 x 34 cm 30€
- [KitBag Mini AVSP25](https://www.aventureverticale.com/en/caving/waist-bag-avsp25.html) 5L oval 11 x 15 x 30 cm 26€
- [Pochette Mini AVSP26](https://www.aventureverticale.com/en/caving/waist-bag-avsp26.html) 4L rectangular 9 x 15 x 30 cm 16€

Los tres modelos de 5L son interesantes, muy similares al Personal 5.
Aunque algunos tienen un cierre de tela similar a los modelos Mini de Landjoff, estos de AV todos tienen un cierre
exterior, por lo que se pueden cerrar aunque se estropee la tela.

La diferencia de precio entre los productos de AV y Landjoff es notable (el doble o casi el triple), pero el
inconveniente para adquirir los productos de Landjoff es que la empresa es búlgara y el envío son 23€, por lo que no
mereca la pena hacer un pedido hasta no estar cuatro o cinco personas para compartir los gastos de envío.
