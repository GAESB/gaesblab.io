# Saca de espalda

En la actividad habitual del grupo, el uso principal de la saca de espalda es el transporte del material personal de casa
al local, al coche, al aparcamiento, durante la aproximación hasta la entrada a la cavidad y de vuelta tras la
exploración.
Sin embargo, en campañas concretas en que se realice vivac, es posible que sea necesario transportarla al interior.
Por lo tanto, en principio cualquier mochila de monte puede utilizarse.
Sin embargo, las pensadas para espeleología suelen estar hechas de PVC/TPU (para que sea más fácil limpiar el barro) y
no tienen elementos colgantes (que puedan engancharse en pasos estrechos).
En caso de utilizar una mochila de tela, hay que usar bolsas de plástico y tener mayor cuidado de no ensuciarlo todo
después de cada actividad; y puede resulta peligroso su transporte al interior.
Las mochilas para barrancos son un punto intermedio: están hechas de PVC/TPU y no tienen elementos colgantes, pero los
orificios para la evacuación del agua pueden engancharse haciendo que se estropee rápido;
además, no suelen tener asas laterales que sí incluyen casi todas las pensadas para espeleología.

Puesto que debemos transportar arneses, material duro, mono, botas, comida, ropa, casco, etc. la saca de espalda debe
tener al menos 40L, siendo preferible que tenga 50-60L.

El producto de referencia en el grupo por su calidad y acabados son los modelos [TRANSPORT 45](https://www.petzl.com/ES/es/Sport/Sacos-y-accesorios/TRANSPORT-45)
y [TRANSPORT 60](https://www.petzl.com/ES/es/Sport/Sacos-y-accesorios/TRANSPORT-60) de [Petzl](https://www.petzl.com).
Entre los modelos para barrancos está el [ALCANADRE GUIDE 45](https://www.petzl.com/ES/es/Sport/Sacos-y-accesorios/ALCANADRE-GUIDE-45).
Durante un tiempo, pudieron adquirirse por 75-100€, pero actualmente están a 110-125€.

[Landjoff](https://landjoff.com) tiene tres modelos interesantes, todos de PVC y disponibles en seis colores:

- [Speleo 42H](https://landjoff.com/product/80/speleo-42h.html) 44€
- [Rucksack SPELEO 42](https://landjoff.com/product/98/rucksack-speleo-42.html) 72€
- [Rucksack SPELEO 56](https://landjoff.com/product/99/rucksack-speleo-56.html) 80€

[Aventure Verticale (AV)](https://www.aventureverticale.com) tiene varios modelos:

- [Kit Bag 40L AVSP36](https://www.aventureverticale.com/en/caving/transport-bag-avsp36.html) 80€
- [Kit Bag 45L AVSP32](https://www.aventureverticale.com/en/caving/transport-bag-avsp32.html) 65€
- [Kit Bag 55L AVSP33](https://www.aventureverticale.com/en/caving/transport-bag-avsp33.html) 95€
- [Kit Bag Comfort 45L AVSP42](https://www.aventureverticale.com/en/caving/transport-bag-avsp42.html) 90€
- [Kit Bag Comfort 55L AVSP43](https://www.aventureverticale.com/en/caving/transport-bag-avsp43.html) 125€
- [Kit Bag Comfort 60L AVSP44](https://www.aventureverticale.com/en/caving/transport-bag-avsp44.html)

RODCLE tiene varios modelos (colores) para barrancos de 35L por 75-80€.

Trango World tiene el modelo [CANYON 45](https://www.trangoworld.com/producto/mochila-canyon-45-naranja-140) por 92€ que
en oferta puede estar a un precio interesante (55-65€).
