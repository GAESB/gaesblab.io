# Sacas

```{note}
Además de las sacas personales (la de cintura y la de espalda/aproximación), es habitual transportar sacas colectivas de
diversos tamaños.
Sin embargo, puesto que esas sacas las aporta el grupo, normalmente junto con cordinos y mosquetones, no se incluyen aquí.
```

```{toctree}
cintura
espalda
```
