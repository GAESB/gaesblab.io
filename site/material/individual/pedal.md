# Pedal

La solución recomendada es el uso de cordino de Dyneema de 5 mm.
Con 3 m puede hacerse un pedal con doble gaza para ambos pies.

Es importante que el cordino del pedal sea una sola pieza y sea cordino cilíndrico, aunque puede utilizarse
opcionalmente cinta para las gazas de los pies.
Esto es debido a que en ciertas maniobras de (auto)rescate el cordino del pedal puede resulta útil, lo que se ve
impedido si es de cinta o si combina cordino y cinta.
Por eso, no son recomendables modelos como Petzl [FOOTAPE](https://www.petzl.com/ES/es/Sport/Bloqueadores/FOOTAPE) o
Camp [TURBOLOOP](https://www.camp.it/d/us/us/outdoor/product/1540).

Asimismo, se recomienda el uso de dos gazas (una para cada pie), ya que se ejerce menos presión en los laterales de los
pies porque el ángulo superior del triángulo de fuerzas es más cerrado.

No se recomiendan modelos de longitud regulable mediante hebilla porque el pedal está sometido a presión frecuentemente
y la hebilla termina fallando.
Es preferible usar un nudo polaco en el mosquetón para ajustar la longitud del pedal.

[Material Técnico de Espeleología (MTDE)](https://www.mtde.net) tiene dos modelos recomendables que combinan cordino
de Dyneema con cinta para la gaza: C1000 Trasion (una sola gaza para un solo pie) y C1010 Siam (dos gazas para ambos pies).
También tiene otros dos modelos no tan recomendables: C1030 Rus (un gran lazo para ambos pies) y C1020 (para cursos, con una
cinta regulable y un cordino más corto).

[Petzl](https://www.petzl.com) tiene [FOOTCORD](https://www.petzl.com/ES/es/Sport/Bloqueadores/FOOTCORD).

RODCLE tiene cuatro modelos: Simple|Doble 120 y Simple|Simple 135+R.

Sin embargo, como sucede con los cabos, el precio del metro de cordino de Dyneema de 5 o 5.5 mm está alrededor de 3.15-4.30€,
mientras que las soluciones comerciales cuestan 20-35€.
Por eso, unido a que la voluminosidad de las gazas de cinta puede resultar molesta cuendo no se está usando el pedal, la
solución recomendada es adquirir 3 m de cordino.

Se usa un nudo simple u ocho con doble gaza para los pies, y un nudo polaco para el extremo del mosquetón.

```{note}
Es difícil encontrar anillos/aros de cinta cosidos de 18-22 cm.
Por lo tanto, si se prefiere el uso de cintas para las gazas de los pies puede ser razonable adquirir una solución
comercial.
En caso de comprar cinta tubular por metros, se deberá coser o anudar, por lo que resultará ligeramente más voluminoso.
```
