# Arnés pélvico

El producto de referencia en el grupo por su relación calidad-precio es el modelo *Amazonia* de [Material Técnico de Espeleología (MTDE)](https://www.mtde.net):
[Réf. B6121](https://www.mtde.net/blog/2010/06/02/amazonia-4/).

El mismo fabricante tiene dos modelos similares: [Picos Réf. B6150](https://www.mtde.net/blog/2010/06/02/picos/) y [Varonia Réf. B6130](https://www.mtde.net/blog/2010/06/02/varonia-2/).
El precio es también similar (+-5€), pero no aportan beneficios significativos en comparación con el Amazonia.

```{important}
El modelo Picos debe ser ajustado muy correctamente para evitar incomodidad, ya que las cintas de los
muslos son notablemente más anchas.
Por eso no es recomendable como primer arnés.
```

[Petzl](https://www.petzl.com) fabrica los modelos [SUPERAVANTI](https://www.petzl.com/ES/es/Sport/Arneses/SUPERAVANTI)
y [AVEN](https://www.petzl.com/ES/es/Sport/Arneses/AVEN), que están a un precio similar o superior a los modelos de MTDE
y no disponen de cinta culera.

[Aventure Verticale (AV)](https://www.aventureverticale.com) tiene una oferta interesante:

- [Mazerin AVCA04](https://www.aventureverticale.com/en/caving/sit-harness-avca04.html) tanto para barrancos como espeleo.
  Salvo en cavidades muy acuáticas y/o verticales, el refuerzo/asiento puede resultar incómodo para la progresión
  horizontal.
- [Muruck AVSP01](https://www.aventureverticale.com/en/caving/sit-harness-avsp01.html) similar al SUPERAVANTI de Petzl,
  con gomas en la parte trasera de los muslos pero sin culera.
- [Tecnibat AVSP02](https://www.aventureverticale.com/en/caving/sit-harness-avsp02.html), similar al modelo Picos de
  MTDE pero sin refuerzo en la cinta culera.
- [Krubera AVSP08](https://www.aventureverticale.com/en/caving/complete-harness-avsp08.html) combina en una sola pieza
  arnés pélvico y de pecho de tipo tirantes.
  La parte inferior es similar al Muruck (en talla única) y la superior al TORSE de Petzl.

```{note}
Es interesante que los modelos Muruck y Tecnibat de AV están disponibles en varias tallas para cinturas desde 55 cm
hasta 150 cm, y muslos desde 35-45 cm hasta 90 cm, mientras que los modelos de MTDE son de talla única.
Por lo tanto, son alternativas interesantes para personas con muy poco o mucho volumen a los que no les queden cómodos
los modelos de MTDE.
Además, el Muruck puede encontrarse un 15% más barato que los modelos de MTDE.
```

[Rock Empire](https://www.rockempire.cz) fabrica un único modelo ([Speleo](https://www.rockempire.cz/en/product/speleo)),
que se ofrece también con maillón semicircular incluido ([Speleo Rapide](https://www.rockempire.cz/en/product/speleo-rapide)).
El diseño es similar al Picos de MTDE.
