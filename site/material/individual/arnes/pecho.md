# Arnés de pecho

A diferencia del arnés pélvico, el de pecho no es crítico para la vida.
Es muy recomendable su uso para mantener el bloqueador ventral en una posición adecuada.
Sin embargo, si fallara o se rompiera, aún sería posible progresar sin que la seguridad se viera comprometida notablemente.
Por eso, existe cierta flexibilidad en la solución utilizada.
Hay principalmente cuatro alternativas:

- Un anillo/aro de cinta cruzado.
- Un anillo/aro de cinta en ocho.
- Arnés de tipo tirantes.
- Arnés completo.

```{note}
Al usar un anillo/aro de cinta es necesario un mosquetón auxiliar para unirla al bloqueador ventral.
```

El uso de un anillo de cinta, sea cruzado o en ocho, no es recomendable salvo que no dispongamos de un arnés específico.
Por el precio de una cinta de 120-150 cm (7.5-12€) podemos adquirir un arnés de tipo tirantes.
Por otro lado, no está de más llevar un anillo de cinta de más por si fuera necesario para alguna instalación o como
arnés de pecho improvisado.

Los modelos de tipo tirantes no tienen mucha variación, salvo en el grosor de la cinta y el tipo de hebilla.
Su función es sólo tensar el bloqueador ventral, y son los mismos tirantes los que lo hacen.
Opcionalmente pueden tener anillos auxiliares para colgar material.

Los modelos completos tienen forma de ocho con una pieza rígida en la cruceta de la espalda y se mantienen en posición
independientemente del tensor del bloqueador ventral, que suele ser una cinta de menor anchura.
Opcionalmente pueden tener anillos/tensor/cierre para mantenerlo unido en el pecho.
Opcionalmente pueden tener anillos auxiliares en el pecho para colgar material.
Opcionalmente pueden tener portamateriales en las costillas.

```{note}
Varios fabricantes sugieren que los modelos completos pueden ser molestos para mujeres, por lo que recomiendan los de
tipo tirantes.
```

[Petzl](https://www.petzl.com) fabrica un modelo de tipo tirantes ([TORSE](https://www.petzl.com/ES/es/Sport/Arneses/TORSE))
y otro de tipo completo.

[Aventure Verticale (AV)](https://www.aventureverticale.com) tiene un modelo simple,
[Speleight AVSP11](https://www.aventureverticale.com/en/caving/shoulder-straps-avsp11.html),
que no es notablemente diferente de un anillo/aro de cinta en ocho.
También tiene dos modelos de tipo completo.

[Material Técnico de Espeleología (MTDE)](https://www.mtde.net) tiene un modelo [Clásico Réf. B6340](https://www.mtde.net/blog/2010/06/02/clasico-2/)
que, al igual que el AV Speleight (AVSP11), no es notablemente diferente de un anillo/aro de cinta en ocho.
También tiene un modelo [Piri Réf. B6330](https://www.mtde.net/blog/2010/06/02/piri-2/) de tirantes con  4 anillos
auxiliares y una cinta para unir la cruceta trasera al mosquetón semicircular (pasando por los riñones), en vez de
unirse al cinturón del arnés pélvico.
Además, tiene un modelo completo.

El fabricante asiático [Xinda](http://www.xindaclimbing.com/), cuyos productos pueden adquirise a través de AliExpress/Alibaba,
dispone de un modelo de [tirantes](https://www.alibaba.com/product-detail/XINDA-high-quality-upper-body-climbing_60775865434.html)
similar al Petzl TORSE, y dos modelos completos.

| Modelo | Cierre | Anillos | Portamaterial |
|---|---|---|---|
| [EXPLO](https://www.petzl.com/ES/es/Sport/Arneses/EXPLO) | No | 2+2 | 2+2 |
| [AV Spelshoulder AVSP12](https://www.aventureverticale.com/en/caving/shoulder-straps-avsp12.html) | No | 3+3 | No |
| [AV Spelshoulder Pro AVSP12R](https://www.aventureverticale.com/en/caving/chest-harness-avsp12r.html) | No | 2 | 2+2 |
| [MTDE Garma Réf. B6320](https://www.mtde.net/blog/2010/06/02/garma-2/) | Anillos | No | 2+1 (cinta) |
| [Xinda simple](https://www.alibaba.com/product-detail/XINDA-black-upper-body-climbing-harness_1600958956158.html) | No | 2+2 | 2+2 (cinta) |
| [Xinda completo](https://www.alibaba.com/product-detail/XINDA-latest-black-upper-body-climbing_62113358453.html) | Anillos | 2+1 | 1+1 |

Dado el precio de los productos de Xinda (10€ el de tirantes y 18-20€ el completo, en ambos casos gastos de envío
incluidos), resulta difícil recomendar otras alternativas.
El Petzl TORSE cuesta 17,5€, el Clásico de MTDE 20€, los AV 27,5-35€ y el MTDE Garma o Petzl EXPLO 50-55€.
El tiempo nos dirá si lo barato sale caro.

```{note}
En caso de usar los modelos MTDE Garma o el Completo de Xinda, es recomendable utilizar un pequeño mosquetón auxiliar
para cerrar el pecho, o llevarlo bien ajustado a los lados.
```
