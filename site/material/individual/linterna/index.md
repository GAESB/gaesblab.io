# Linternas

La iluminación principal es la linterna frontal, para la que llevaremos siempre un juego de pilas de repuesto además de
los que tengamos pensado utilizar durante una actividad.
Sin embargo, por un lado en ocasiones querremos mirar algún pozo u orificio donde la posición de la linterna en el casco
no nos resulte útil y necesitemos una linterna en la mano.
Por otro lado, es posible que la frontal principal se nos estropee, se golpee o se caiga.
Por ello, siempre llevaremos una segunda linterna, y será interesante poder montarla en el casco.
Puede ser una segunda frontal con la misma sujección que la principal o con cintas elásticas, o una linterna de mano con
un soporte en el lateral del casco.

Es útil que la segunda frontal o la linterna de mano sean compatibles con el mismo tipo de pila/batería que la frontal
principal, ya que nos permitirá ahorrar espacio al transportar una/un juego dentro de la misma.

```{note}
Del mismo modo que al progresar por cuerda utilizamos al menos tres puntos de seguridad (ambos cabos y el descensor o el
bloqueador ventral, dependiendo del sentido), para que al maniobrar con uno de ellos sigamos estando con dos, no está de
más llevar una tercera linterna.
De otra forma, en cuanto fallara la principal deberíamos emprender el camino de salida, ya que no tendríamos reserva.
La tercera linterna debería usarse en casos muy excepcionales, por lo que prácticamente cualquiera sirve.
```

```{toctree}
frontal
mano
bateria
```
