# Linterna frontal

La linterna frontal, que es la iluminación principal, es el elemento en el que más variedad de características y precios
hay.
Mientras que los arneses, bloqueadores, descensor, mosquetones, cascos, sacas y monos tienen un rango muy limitado de
características que variar, en las linternas frontales el número y potencia de los LEDs ofrecen un abanico mucho más
amplio.
Desde 20€ hasta 600€, depende de cuánto queramos gastar.

Para la progresión individual no es necesario un gran alcance o una gran potencia, particularmente porque tratamos de
acostumbrar la vista a la potencia mínima para alargar la duración.
Sin embargo, en grandes cavidades y durante la exploración, la potencia adicional ayuda a entender mejor la morfología.
Menos de 500 lúmenes son insuficientes, siendo recomendable 1000 lúmenes al menos.

Es recomendable que tenga modos de "corta" y "larga", ya que dependiendo de la situación querremos iluminar más o menos,
más cerca o más lejos.
Esto normalmente se consigue mediante múltiples LEDs y lentes con interruptores diferenciados.

Es muy importante que las baterías/pilas sean extraíbles, de forma que podamos llevar de repuesto por si se agotaran en
mitad de la exploración.
Puesto que es habitual el uso de pilas recargables de tipo 18650 o 21700, es recomendable adquirir frontales de ese tipo,
ya que en caso de emergencia nos pueden dejar pilas cargadas.
Si el frontal escogido tiene un tipo de batería/pila particular, deberemos asegurarnos de tener recambios ya que siempre
llevaremos un juego de baterías/pilas cargadas adicional a lo que esperemos usar.

## Fenix

Aunque no son específicos para espeleología, en el rango menor a 200€ son interesantes por su relación potencia-precio
los productos de [Fenix](https://www.fenixlight.com/):

| Modelo | Batería | Lúmenes | Distancia (m) | Duración (h) | Precio |
|---|---|---|---|---|---|
| [HM60R](https://www.fenixlight.com/product/detail/index.php?id=174) | 18650/CR123A (incl. ARB-L18-3400) | 1300 | 120 | 300 | 72€ |
| [HM61R V2.0](https://www.fenixlight.com/product/detail/index.php?id=208) | 18650/CR123A (incl. ARB-L18-3400) | 1600 | 162 | 300 | 90€ |
| [HM65R](https://www.fenixlight.com/product/detail/index.php?id=12) | 18650/CR123A (incl. ARB-L18-3400) | 1400 | 163 | 280 | 85€ |
| [HM70R](https://www.fenixlight.com/product/detail/index.php?id=169) | 21700 o ALF-18+18650 (incl. ARB-L21-5000) | 1600 | 186 | 400 | 100€ |
| [HM71R](https://www.fenixlight.com/product/detail/index.php?id=210) | 21700 (incl. ARB-L21-5000 V2.0) | 2700 | 230 | 400 | 120€ |
| [HP25R V2.0](https://www.fenixlight.com/product/detail/index.php?id=165) | 21700 o 18650 (incl. ARB-L21-5000) | 1600 | 290 | 400 | 120€ |
| [HP30R V2.0](https://www.fenixlight.com/product/detail/index.php?id=175) | 21700 (incl. 2xARB-L21-5000) | 3000 | 270 | 120 | 205€ |
| [ARB-L18-3400](https://www.fenixlight.com/product/detail/index.php?id=106) | - | - | - | - | 20€ |
| [ARB-L21-5000 V2.0](https://www.fenixlight.com/product/detail/index.php?id=197) | - | - | - | - | 25€ |
| [ALF-18](https://www.fenixlight.com/product/detail/index.php?id=129) | - | - | - | - | 6€ |

Existen las variantes [HM65R-T](https://www.fenixlight.com/product/detail/index.php?id=25) y [HM65R-DT](https://www.fenixlight.com/product/detail/index.php?id=217),
que reemplazan algunos de los LEDs enfocandos en su uso para Trail; por lo que no resultan particularmente interesantes
para espeleología.
Tampoco es interesante el HM61R, porque no tiene luces cortas y largas.

Los modelos HM60R, HM65R, HM70Ry  HM71R no tienen petaca trasera, sino que la batería está integrada en el frontal.

Entre el HM60R y el HM65R parece merecer optar por el segundo, ya que además del alcance adicional la carcasa parece
construida con mejores materiales.
Entre la HM65R y la HM70R la diferencia principal es la autonomía, al traer 21700 la segunda.
Sin embargo, con respecto a la facilidad de uso, la HM65R tiene botones independientes para las dos luces, mientras que
el HM70R tiene uno solo y la funcionalidad depende del número y duración de las pulsaciones, lo que puede resultar
incómodo en situaciones de tensión.
Además, la HM65R tiene tres modos de luz corta (8 lumens 280h, 130 lumens 48h y 400 lumens 20h), mientras que el HM70R
sólo tiene dos (70 lumens 48h y 400 lumens 8h), porque el de más bajo consumo es de luz roja (5 lumens 400h).

Entre los modelos de 120€ es llamativo que mientras el HM70R se especifica que es compatible con 18650 usando el adaptador
ALF-18, en el HM71R no se indica nada, y en el HP25R se indica que es compatible pero no se especifica si requiere
adaptador.

Como referencia, en fenixlinternas.com:

| Producto | Referencia | Precio |
|---|---|---|
| Frontal Fénix HM70R con 1600 Lúmenes Recargable con batería 21700 | SKU:HM70R | 99,90€ |
| Adaptador de batería ALF-18 para usar 18650 en linternas que utilizan la 21700 | SKU:ALF-18 | 5,95€ |
| Accesorio ALG-03-V2.0 para HL55 / HL60R / HM60R / HM61R / HM65R / HM70R | SKU:ALG-03-V2.0 | 6,50€ |
| Total (envio gratuito) | | 112,35€ |

En machay.es:

| Producto | Referencia | Precio |
|---|---|---|
| Frontal HM65R Fenix 1400 lumens | HM65R | 85,47€ |
| SOPORTE PARA CASCO FRONTAL FENIX | ALG-03-V2.0 | 7,60€ |
| Total (envio gratuito) | | 93,07€ |

```{note}
Varios miembros del grupo tienen el modelo HM65R, y otros el HP25R o HP30R.
Sin embargo, compraron la primera versión de [HP25R](https://www.fenixlighting.com/products/fenix-hp25r-rechargeable-headlamp) y [HP30R](https://www.fenixlighting.com/products/fenix-hp30r-rechargeable-headlamp)
hace 2-3 años, cuando usaban una o dos baterías 18650 de 2600 mAh, y antes de que se encarecieran 100€ o más.

Al precio al que está la HP30R V2.0, empieza a ser razonable valorar otros modelos más específicos para espeleología.
Aunque, como se indica más adelante, el rango entre 200€ y 400€ parece estar poco cubierto actualmente.
```

## El Speleo

De origen croata, producción de pequeña escala y especializado en espeleología, durante la última década ha sido un
referente en el rango entre 200€ y 400€.

El modelo SAR (Search and Rescue), por 200-250€ ofrecía 3000 lúmenes combinados, 2x18650 con petaca trasera, montaje
frontal tipo GoPro y carcasa de aluminio y policarbonato.
Los últimos modelos fueron LUNATIC 2500 (420€) y TERRA 1600 (260€).

Todos los modelos se caracterizaban por tratar de reducir la radiación o ruido magnético, para no interferir con los
aparatos de medida.
También eran aptas para buceo hasta -100m.

Lamentablemente, parecen no estar disponibles desde después de la pandemia.

## Earthworm

[earthwormcavelight.co.uk](https://earthwormcavelight.co.uk/), del Reino Unido, también de producción de pequeña escala
y especializado en espeleología, tiene productos en el rango entre 250€ y 450€, a medio camino entre El Speleo y Scurion.

Actualmente disponen del modelo Earthworm 3, en tres variantes: SOLO (£225), DUAL (£325) y MULTI (£399).

Parece no haber mucha información disponible sobre el tipo de baterías y conectores que utilizan.
Tampoco se especifica que reduzcan la radiación/ruido o no son aptas para buceo.

## Little Monkey

[littlemonkeycaving.co.uk](https://littlemonkeycaving.co.uk/), también del Reino Unido, también de producción de pequeña
escala y especializado en espeleología, tiene un solo modelo actualmente, el [Rude Nora 4](https://littlemonkeycaving.co.uk/Rude-Nora-4/).
El kit completo (incluyendo batería y cargador) está a £369 y envío sólo al Reino Unido: [littlemonkeycaving.co.uk/Nora-4-Lamps](https://littlemonkeycaving.co.uk/Nora-4-Lamps/).
El frontal solo está a £189 y se hacen envíos internacionales.
Parece que anteriormente vendían la carcasa/petaca para la batería por separado, pero ya no está disponible: [littlemonkeycaving.co.uk/Battery-Boxes-1](https://littlemonkeycaving.co.uk/Battery-Boxes-1/).
Por lo tanto, la diferencia principal con el resto de modelos es que, salvo que se adquiera en el Reino Unido, hay que
comprar la linterna, la batería y la carcasa/petaca por separado.
Se trata de una solución interesante para quienes quieran una frontal robusta y potente, y deseen ajustar el sistema a
su equipo y necesidades particulares.
Sin embargo, no es una solución tan atractiva para quienes quieran algo listo para usar.

## Scurion

De origen suizo y especializada, es uno de los más longevos y el más profesional de los productores de frontales LED
para espeleología a pequeña escala.

Tienen tres modelos (700, 900, 1500), de los cuáles el básico cuesta a partir de 425€.
Hay una [comparativa](https://www.scurion.ch/jm19/en/lamps/model-overview/scurion-900.html?view=article&id=47:scurion-r-lamp-history&catid=37:infos-en-gb)
de los modelos descatalogados y los actuales.
Tienen variedad de [opciones](https://www.scurion.ch/jm19/en/lamps/options.html): luz UV, luz roja, lente para proyectar
el estado de la batería en la mano/pared, [soporte para cascos](https://www.scurion.ch/jm19/en/?view=article&id=125:scurion-r-blade-mount-system-sbs-for-helmets&catid=23:accessories) VERTEX,
[luz cálida](https://www.scurion.ch/jm19/en/lamps/model-overview/warm-white-option.html),
color del aluminio de la carcasa, etc.

En cuanto a baterías, parece que tienen su propia solución (ver [Batteries and Charger](https://www.scurion.ch/jm19/en/?view=article&id=57:batteries-and-charger&catid=23:accessories)
y [Battery module system](https://www.scurion.ch/jm19/en/?view=article&id=113:battery-module-system&catid=23:accessories)),
el pack de 4 celdas cuesta 100-125€ y no resulta tan obvio encontrar reemplazos de otras fuentes o utilizar 18650 en
caso de necesidad.
Por otro lado, con la potencia que manejan estos modelos, es de esperar que las baterías tengan cierta electrónica y no
sean sólo pilas en serie/paralelo.

Tienen detalles de diseño que muestran la calidad, como el control de LEDs por corriente constante (para evitar que el
brillo varíe en función del estado de carga de la batería), o el uso de un interruptor no magnético para evitar la
interferencia con aparatos de medida.

Son aptas para buceo hasta -150m o -250m, dependiendo del tipo de carcasa.

Puede ser interesante para personas experimentadas y exploraciones muy largas y exigentes, pero resulta excesivo para un
primer equipo, ya que supondría invertir en la frontal más que en todo el resto del material.
