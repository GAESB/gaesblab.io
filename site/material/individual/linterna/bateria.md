# Batería

## 18650

| Modelo | Tensión nom. (V) | Capacidad (mAh) | Peso (g) | Longitud (mm) | Diámetro (mm) | Garantia (meses) | Ciclos de carga | Carga (A) | Descarga (A) | Precio |
|---|---|---|---|---|---|---|---|---|---|---|
| Fenix [ARB-L18-2600](https://www.fenixlighting.com/products/fenix-arb-l18-2600-rechargeable-18650-battery) | 3.6 | 2600 | 50 | 69 | 18.6 | 12 | 500 | 1, max. 2 | - | 13-14.5€ |
| Fenix [ARB-L18-3400](https://www.fenixlighting.com/products/fenix-arb-l18-3400-rechargeable-18650-battery) | 3.6 | 3400 | 50 | 69 | 18.6 | 12 | 500 | 1, max. 2 | - | 19-20€ |
| Fenix [ARB-L18-3500](https://www.fenixlighting.com/products/fenix-arb-l18-3500-rechargeable-18650-battery) | 3.6 | 3500 | 50 | 69 | 18.6 | 12 | 500 | 1, max. 2 | - | 23-25€ |
| FullWatt [LIR18650-26-CIT](https://www.ukai.com/es-es/pilas-y-bater%c3%adas/fullwat-lir18650-26-cit-bater%c3%ada-recargable-lir18650--26--cit) | 3.6 | 2600 | 48 | 70 | 18.5 | 6 | - | max. 2.6 | max. 3 | 9-9.5€ |
| FullWatt [LIR18650-34-CIT](https://www.ukai.com/es-es/pilas-y-bater%c3%adas/fullwat-lir18650-34-cit-bater%c3%ada-recargable-lir18650--34--cit) | 3.7 | 3400 | 49 | 70 | 18.5 | 6 | - | max. 3.4 | max. 3 | 11.50-12€ |

Los modelos de Fenix, además de circuitería de protección ante cortocircuitos, tienen orificios para liberar la presión
y evitar explosiones en caso de mal funcionamiento.
Tienen variantes de los tres modelos con conector micro-USB incorporado en la batería/pila (ARB-L18-2600U, ARB-L18-3400U,
y ARB-L18-3500U).
Son 1 mm más largos, pero por lo demás tienen las mismas características que los modelos *normales*.

FullWatt tiene modelos sin protección LIR18650-26 y LIR18650-34, 5 mm más cortos (de 65mm, como indica la denominación)
y con corriente de descarga mayor.

```{note}
De acuerdo con [blog.fullwat.com/baterias-litio-falsas](http://blog.fullwat.com/baterias-litio-falsas/) y
[blog.fullwat.com/baterias-de-litio-falsas-parte-2/](http://blog.fullwat.com/baterias-de-litio-falsas-parte-2/),
los elementos principales para detectar baterías 18650 falsas son:
- La capacidad máxima real es de 3400-3500 mAh. Para mayor capacidad se necesita más espacio.
- El peso debe ser de aproximadamente 50g.
Por lo demás, es necesario hacer pruebas de uso y/o destructivas.
```

### Almacenamiento

- [aliexpress.com/item/1005002279071811](https://es.aliexpress.com/item/1005002279071811.html)
  - 2x18650 1.56€ + 1.86€ (gratis sobre 10€)
  - 4x18650 2.27€ + 1.86€ (gratis sobre 10€)
  - 8x18650 3.88€ + 1.86€ (gratis sobre 10€)
