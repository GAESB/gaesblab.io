# Linterna de mano

Tal como se indica en {doc}`index`, es útil llevar una segunda linterna que podamos usar con la mano o como
reemplazo de la principal en caso de necesidad, y que preferiblemente sea compatible con el mismo tipo de pila/batería
que la {doc}`frontal` principal.

Por ejemplo, el modelo UltraFire [WF-502B](https://ultrafire.es/linternas-de-mano-tacticas/523-ultrafire-wf-502b-8944602688520.html#/34-opciones-5_modos) se alimenta con una 18650 y en su variante de 5 modos está disponible por 20€.
El modelo [UF-N9](https://ultrafire.es/linternas-zoom-de-enfoque-regulable/114-ultrafire-zoom-uf-n9-de-600lm-8944602310766.html)
también se alimenta con una 18650, tiene sólo 3 modos y está disponible por 12€.
Cualquiera de ellas, o modelos equivalentes de origen asiático, complementan el frontal HM65R de Fenix, o el HM70R con
el adaptador ALF-18, o los modelos de El Speleo con 2x18650.
