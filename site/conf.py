# -*- coding: utf-8 -*-
#
# Authors:
#   Unai Martinez-Corral
#     <unai.martinezcorral@ehu.eus>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0


from pathlib import Path


ROOT = Path(__file__).resolve().parent


# -- General configuration ------------------------------------------------

numfig = True

extensions = [
    'sphinx.ext.extlinks',
    'sphinx.ext.intersphinx',
#    'sphinxcontrib.bibtex',
    'myst_parser',
    'sphinx_design',
]

myst_enable_extensions = ["colon_fence", "attrs_inline"]

#bibtex_default_style = 'plain'
#bibfiles = [
#    ROOT.parent / 'refs.bib',
#]
#bibtex_bibfiles = [str(item) for item in bibfiles]
#for item in bibfiles:
#    if not item.exists():
#        raise Exception(f"Bibliography file {item} does not exist!")

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

master_doc = 'index'

project = u'GAESB'
copyright = u'2023-2024'
author = u'Contributors'

version = "latest"
release = version  # The full version, including alpha/beta/rc tags.

language = 'es'

# reST settings
prologPath = "prolog.inc"
try:
    with open(prologPath, "r") as prologFile:
        rst_prolog = prologFile.read()
except Exception as ex:
    print("[ERROR:] While reading '{0!s}'.".format(prologPath))
    print(ex)
    rst_prolog = ""

# -- Options for HTML output ----------------------------------------------

html_theme = "furo"

html_css_files = [
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/fontawesome.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/solid.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/brands.min.css",
]

html_theme_options = {
    "source_repository": "https://gitlab.com/GAESB/gaesb.gitlab.io",
    "source_branch": "main",
    "source_directory": "doc/sphinx",
    "sidebar_hide_name": True,
    "footer_icons": [
        {
            "name": "Unión de Espeleólogos Vascos | Euskal Espeleologoen Elkargoa",
            "url": "https://euskalespeleo.com/",
            "html": "",
            "class": "fa-solid fa-building-columns",
        },
        {
            "name": "espeleo-gaes.blogspot.com",
            "url": "https://espeleo-gaes.blogspot.com",
            "html": "",
            "class": "fa-solid fa-building",
        },
        {
            "name": "GitLab",
            "url": "https://gitlab.com/GAESB",
            "html": "",
            "class": "fa-solid fa-brands fa-gitlab",
        },
    ],
}

html_static_path = ['_static']

html_extra_path = [str(Path(__file__).resolve().parent / 'public')]

html_logo = str(Path(html_static_path[0]) / "img/logo.png")
html_favicon = str(Path(html_static_path[0]) / "img/favicon.png")

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    'papersize': 'a4paper',
}

latex_documents = [
    (master_doc, 'GAESB.tex', u'GAES de Bilbao', author, 'manual'),
]

# -- Options for Texinfo output -------------------------------------------

texinfo_documents = [
  (master_doc, 'GAESB', u'GAES de Bilbao', author, 'GAESB', 'Sport.', 'Miscellaneous'),
]

# -- Sphinx.Ext.InterSphinx -----------------------------------------------

intersphinx_mapping = {
   'python': ('https://docs.python.org/3/', None),
   'TED': ('https://euskalespeleo.gitlab.io/ted/', None),
}

# -- Sphinx.Ext.ExtLinks --------------------------------------------------
extlinks = {
   'wikipedia':      ('https://en.wikipedia.org/wiki/%s', 'w:%s'),
   'youtube':        ('https://www.youtube.com/watch?v=%s', 'yt:%s'),
   "web":            ("https://%s", '%s'),
   "gh":             ("https://github.com/%s", 'gh:%s'),
   "gl":             ("https://gitlab.com/%s", 'gl:%s'),
   "glmerge":        ("https://gitlab.com/GAESB/gaesb.gitlab.io/-/merge_requests/%s", '!%s'),
   "glsharp":        ("https://gitlab.com/GAESB/gaesb.gitlab.io/-/issues/%s", '#%s'),
   "glissue":        ("https://gitlab.com/GAESB/gaesb.gitlab.io/-/issues/%s", 'issue #%s'),
   "glsrc":          ("https://gitlab.com/GAESB/gaesb.gitlab.io/-/blob/main/%s", '%s'),
}
