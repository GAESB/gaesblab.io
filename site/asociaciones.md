# Asociaciones

Con el tiempo, hemos ido haciendo muchas amistades y contactos en el mundo de la espeleo y actualmente pertenecemos y
colaboramos con entidades como:

- Unión de Espeleólogos Vascos / Euskal Espeleologoen Elkargoa (U.E.V./E.E.E.)
  - [euskalespeleo.com](https://euskalespeleo.com)
- AXPEA (Asociación Vizcaína de Estudios Espeleológicos)
- Association pour la Recherche Spéléologique Intenationale à la Pierre Saint-Martin (A.R.S.I.P.)
  - [arsip.fr](https://www.arsip.fr/)
