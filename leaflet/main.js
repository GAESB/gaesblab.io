/*
Authors:
	Aitor Oriñuela Salaverria
	Unai Martinez-Corral <unai.martinezcorral@ehu.eus>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

SPDX-License-Identifier: Apache-2.0
*/

/*
	Base layers
*/

var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '© OpenStreetMap'
});

/*
Hay 9 capas en https://geo.bizkaia.eus/arcgisserverinspire/services/Kartografia_Cartografia/WMS_Ortoargazkiak_BFA/MapServer/WMSServer?request=GetCapabilities&service=WMS
Parecen ser las mismas que se listan como ORTO_BFA_ en https://geo.bizkaia.eus/arcgisserverinspire/rest/services/Kartografia_Cartografia.
También se hace referencia a ORTO_EJ_1945, ORTO_EJ_1977 y ORTO_EJ_2022.

Por un lado, no parece del todo adecuado identificarlas por el número de capa, ya que si se añaden nuevas podría cambiar
el orden.
Sería recomendable poder identificar la capa en función del campo 'Title', en vez de 'Name'.

Por otro lado, en el visor de https://www.bizkaia.eus/es/cartografia-y-ortofotos, seleccionando Ortofotos del municipio
Urduliz hay resultados anuales desde 2004, y nueve resultados entre 1956 y 2004.
Es posible que los resultados entre 2004 y 2022 sean los mismos que los de geoEuskadi, y por eso no estén disponibles a
través del WMS de la Diputación y sólo ser sirvan los nueve resultados anteriores.
Sin embargo, geoEuskadi incluye también resultados de 2002, 2001, 1991, 1984-1985, 1977-1978, 1956-1957 y 1945-1946, y
las versiones de 2002 de geoEuskadi (ORTO_2002) y de la Diputación (ORTO_BFA_2002) no son las mismas.
Por lo tanto, habría que comprobar si ORTO_EJ_1945, ORTO_EJ_1977 y ORTO_EJ_2022 corresponden a ORTO_1945_46_AMERICANO,
ORTO_1956_57_AMERICANO y ORTO_2022, respectivamente.
*/

var ortofotos_diputacion = {};
for (const [key, value] of Object.entries({
	"Ortofoto - 2002 DFB": 0, //ORTO_BFA_2002
	"Ortofoto - 1999 DFB": 1, //ORTO_BFA_1999
	"Ortofoto - 1995 DFB": 2, //ORTO_BFA_1995
	"Ortofoto - 1990 DFB": 3, //ORTO_BFA_1990
	"Ortofoto - 1983 DFB": 4, //ORTO_BFA_1983
	"Ortofoto - 1975 DFB (parcial)": 5, //ORTO_BFA_1975
	"Ortofoto - 1970 DFB": 6, //ORTO_BFA_1970
	"Ortofoto - 1965 DFB": 7, //ORTO_BFA_1965
	"Ortofoto - 1956 DFB": 8, //ORTO_BFA_1956
})) {
	ortofotos_diputacion[key] = L.tileLayer.wms('https://geo.bizkaia.eus/arcgisserverinspire/services/Kartografia_Cartografia/WMS_Ortoargazkiak_BFA/MapServer/WMSServer?', {
		layers: value,
		attribution: '&copy <a href="https://www.bizkaia.eus/es/inspirebizkaia#wms">DFB</a>'
	});
}

var ortofotos_euskadi = {};
for (const [key, value] of Object.entries({
	"Ortofoto - Más actualizada geoEuskadi": "ORTO_EGUNERATUENA_MAS_ACTUALIZADA",
	//"Ortofoto - 2022 geoEuskadi": "ORTO_2022",
	"Ortofoto - 2021 geoEuskadi": "ORTO_2021",
	"Ortofoto - 2020 geoEuskadi": "ORTO_2020",
	"Ortofoto - 2019 geoEuskadi": "ORTO_2019",
	"Ortofoto - 2018 geoEuskadi": "ORTO_2018",
	"Ortofoto - 2017 geoEuskadi": "ORTO_2017",
	"Ortofoto - 2016 geoEuskadi": "ORTO_2016",
	"Ortofoto - 2015 geoEuskadi": "ORTO_2015",
	"Ortofoto - 2014 geoEuskadi": "ORTO_2014",
	"Ortofoto - 2013 geoEuskadi": "ORTO_2013",
	"Ortofoto - 2012 geoEuskadi": "ORTO_2012",
	"Ortofoto - 2011 geoEuskadi": "ORTO_2011",
	"Ortofoto - 2010 geoEuskadi": "ORTO_2010",
	"Ortofoto - 2009 geoEuskadi": "ORTO_2009",
	"Ortofoto - 2008 geoEuskadi": "ORTO_2008",
	"Ortofoto - 2007 geoEuskadi": "ORTO_2007",
	"Ortofoto - 2006 geoEuskadi": "ORTO_2006",
	"Ortofoto - 2005 geoEuskadi": "ORTO_2005",
	"Ortofoto - 2004 geoEuskadi": "ORTO_2004",
	"Ortofoto - 2002 geoEuskadi": "ORTO_2002",
	"Ortofoto - 2001 geoEuskadi": "ORTO_2001",
	"Ortofoto - 1991 geoEuskadi": "ORTO_1991",
	"Ortofoto - 1984-1985 geoEuskadi": "ORTO_1984_85",
	"Ortofoto - 1977-1978 geoEuskadi (interministerial)": "ORTO_INTERMINISTERIAL_1977_78",
	"Ortofoto - 1956-1957 geoEuskadi (americano)": "ORTO_1956_57_AMERICANO",
  "Ortofoto - 1945-1946 geoEuskadi (americano)": "ORTO_1945_46_AMERICANO",
})) {
	ortofotos_euskadi[key] = L.tileLayer.wms('http://www.geo.euskadi.eus/WMS_ORTOARGAZKIAK', {
		layers: value,
		attribution: '&copy <a href="https://www.geo.euskadi.eus/inicio">Geoeuskadi</a>'
	});
}

/*
	Create map and controls
*/

var map = L.map('map', {
	center: [43.372109, -2.959880],
	zoom: 16,
	layers: [osm]
});

L.control.layers(
	Object.assign(
		{
			"OpenStreetMap": osm,
		},
		ortofotos_euskadi,
		ortofotos_diputacion,
	),
).addTo(map);
